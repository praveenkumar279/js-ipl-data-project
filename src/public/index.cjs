fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const matchesPerYear = Object.entries(data);
    console.log(matchesPerYear);

    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Matches played per year in IPL'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'No of  Matches Played'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Matches played: <b>{point.y} Matches</b>'
      },
      series: [{
        name: 'Matches per year',
        data: matchesPerYear,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y}', // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }]
    })
  });





fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {

    const teamWins = {};

    Object.entries(data).map(([teamName, winData]) => {
      const years = Object.keys(winData).map(year => parseInt(year));
      const winsArray = years.reduce((acc, year) => {
        const winCount = winData.hasOwnProperty(year.toString()) ? winData[year.toString()] : null;
        return [...acc, winCount];
      }, []);
      teamWins[teamName.toLowerCase()] = winsArray;
    });

    console.log(teamWins);

    Highcharts.chart('container-2', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Matches won per team per year',
        align: 'center'
      },

      xAxis: {
        categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2014, 2016, 2017],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Matches',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'sunrisers hyderabad',
        data: teamWins['sunrisers hyderabad']
      }, {
        name: 'rising pune supergiant',
        data: teamWins['rising pune supergiant']
      }, {
        name: 'kolkata knight riders',
        data: teamWins['kolkata knight riders']
      }, {
        name: 'kings xi punjab',
        data: teamWins['kings xi punjab']
      },
      {
        name: 'royal challengers bangalore',
        data: teamWins['royal challengers bangalore']
      }, {
        name: 'mumbai indians',
        data: teamWins['mumbai indians']
      }, {
        name: 'delhi daredevils',
        data: teamWins['delhi daredevils']
      }, {
        name: 'gujarat lions',
        data: teamWins['gujarat lions']
      },
      {
        name: 'rajasthan royals',
        data: teamWins['rajasthan royals']
      }, {
        name: 'deccan chargers',
        data: teamWins['deccan chargers']
      }, {
        name: 'pune warriors',
        data: teamWins['pune warriors']
      }, {
        name: 'kochi tuskers kerala',
        data: teamWins['kochi tuskers kerala']
      }, {
        name: 'rising pune supergiants',
        data: teamWins['rising pune supergiants']
      }]
    })
  });





fetch("./output/3-extra-runs-conceded-per-team.json")
  .then((data) => data.json())
  .then((data) => {
    const iplTeams = Object.keys(data);
    const extraRunsConceded = Object.values(data);

    Highcharts.chart('container-3', {
      title: {
        text: 'Extra Runs conceded per team in 2016',
        align: 'center'
      },
      xAxis: {
        categories: iplTeams
      },
      series: [{
        type: 'column',
        name: 'ExtraRuns Conceded',
        colorByPoint: true,
        data: extraRunsConceded,
        showInLegend: false
      }]
    });
  });






fetch("./output/4-top-10-economical-bowlers-in-2015.json")
  .then((data) => data.json())
  .then((data) => {
    const bowlerName = Object.keys(data);
    const bowlerEconomy = Object.values(data);

    const chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container-4',
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 15,
          beta: 15,
          depth: 50,
          viewDistance: 25
        }
      },
      xAxis: {
        categories: bowlerName
      },
      yAxis: {
        title: {
          enabled: false
        }
      },
      tooltip: {
        headerFormat: '<b>{point.key}</b><br>',
        pointFormat: 'Economy {point.y}'
      },
      title: {
        text: 'Top 10 Economy bowlers in 2015',
        align: 'center'
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      series: [{
        data: bowlerEconomy,
        colorByPoint: true
      }]
    });

    function showValues() {
      document.getElementById('alpha-value').innerHTML = chart.options.chart.options3d.alpha;
      document.getElementById('beta-value').innerHTML = chart.options.chart.options3d.beta;
      document.getElementById('depth-value').innerHTML = chart.options.chart.options3d.depth;
    }

    // Activate the sliders
    document.querySelectorAll('#sliders input').forEach(input => input.addEventListener('input', e => {
      chart.options.chart.options3d[e.target.id] = parseFloat(e.target.value);
      showValues();
      chart.redraw(false);
    }));

    showValues();
  });



fetch("./output/5-won-won-match-won-team.json")
  .then((data) => data.json())
  .then((data) => {
    const teams = Object.entries(data);
    Highcharts.chart('container-5', {
      chart: {
        type: 'columnpyramid'
      },
      title: {
        text: 'Won toss and Won match in IPL'
      },
      colors: ['#C79D6D', '#B5927B', '#CE9B84', '#B7A58C', '#C7A58C'],
      xAxis: {
        crosshair: true,
        labels: {
          style: {
            fontSize: '14px'
          }
        },
        type: 'category'
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Won'
        }
      },
      tooltip: {
        valueSuffix: ' m'
      },
      series: [{
        name: 'Won',
        colorByPoint: true,
        data: teams,
        showInLegend: false
      }]
    });
  });



fetch("./output/6-highest-player-of-match-awards-in-season.json")
  .then((data) => data.json())
  .then((data) => {
    const result = Object.entries(data).map(([year, { highestPlayerMatchAwards, Awards }]) => ({
      name: `${year} - ${highestPlayerMatchAwards}`,
      awards: Awards
    }));
    console.log(result);
    const yearAndPlayer = result.map((match) => {
      return match.name
    });
    const NoOfAwards = result.map((match) => {
      return match.awards
    })

    console.log(yearAndPlayer);
    console.log(NoOfAwards);


    Highcharts.chart('container-6', {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 25,
          depth: 70
        }
      },
      title: {
        text: 'Highest Player of the match awards by Season',
        align: 'center'
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      xAxis: {
        categories: yearAndPlayer,
        labels: {
          skew3d: true,
          style: {
            fontSize: '16px'
          }
        }
      },
      yAxis: {
        title: {
          text: 'No of Awards',
          margin: 20
        }
      },
      tooltip: {
        valueSuffix: ' Awards'
      },
      series: [{
        name: 'Player of the match',
        data: NoOfAwards
      }]
    });
  });




fetch("./output/7-batsman-strike-rate-of-each-season.json")
  .then((data) => data.json())
  .then((data) => {

    const batsmanStrikeRate = {};

    Object.entries(data).map(([teamName, winData]) => {
      const years = Object.keys(winData).map(year => parseInt(year));
      const winsArray = years.reduce((acc, year) => {
        const winCount = winData.hasOwnProperty(year.toString()) ? winData[year.toString()] : null;
        return [...acc, winCount];
      }, []);
      batsmanStrikeRate[teamName.toLowerCase()] = {
        name: teamName,
        data: winsArray
      };
    });

    console.log(Object.values(batsmanStrikeRate));


    Highcharts.chart('container-7', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Batsman strike rate of each season',
        align: 'center'
      },
      xAxis: {
        categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Strike rate',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: Object.values(batsmanStrikeRate)
    })
  });





fetch("./output/8-highest-number-times-one-player-dismissed-by-another-player.json")
  .then((data) => data.json())
  .then((data) => {
    const result = Object.entries(data).map(([batsman, { dismissed_by, NoOfTimesDismissed }]) => ({
      name: `${batsman} - ${dismissed_by}`,
      noOfTimesDismissed: NoOfTimesDismissed
    }));

    const playerAndBowler = result.map((match) => {
      return match.name
    });

    const dismissed = result.map((match) => {
      return match.noOfTimesDismissed
    })

    console.log(playerAndBowler);
    console.log(dismissed);

    Highcharts.chart('container-8', {
      title: {
        text: 'Highest number of times one player dismissed by another player',
        align: 'center'
      },
      xAxis: {
        categories: playerAndBowler
      },
      series: [{
        type: 'column',
        name: 'Dismissed',
        colorByPoint: true,
        data: dismissed,
        showInLegend: false
      }]
    });
  });






fetch("./output/9-bowler-with-best-economy-in-super-overs.json")
  .then((data) => data.json())
  .then((data) => {
    const bestEconomyBowler = Object.entries(data);
    console.log(bestEconomyBowler);

    Highcharts.chart('container-9', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Best Economy bowler in super overs'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Economy rate'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Best Economy: <b>{point.y}</b>'
      },
      series: [{
        name: 'Matches per year',
        data: bestEconomyBowler,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y}', // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }]
    })
  });
