const fs = require('fs');
const csv = require('csv-parser');

const iplDeliveriesData = [];
const matchIdOfYear2015 = []

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => {
    if(data.season === '2015'){
        matchIdOfYear2015.push(data.id);
    }
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data',(data) => iplDeliveriesData.push(data))
    .on('end',() => {
        top10EconomyBowlers(iplDeliveriesData,matchIdOfYear2015);
    })
  });


function top10EconomyBowlers(iplDeliveriesData,matchIdOfYear2015){

    const topEconomyBowlersRunsAndBalls = {};
    const topEconomyBowlers = {};

    for(const deliveryOfBall of iplDeliveriesData){
        const matchId = deliveryOfBall.match_id;
        const bowler = deliveryOfBall.bowler;
        const runsInBall = deliveryOfBall.total_runs - deliveryOfBall.legbye_runs - deliveryOfBall.bye_runs;

        if(matchIdOfYear2015.includes(matchId)){
            if(!topEconomyBowlersRunsAndBalls[bowler]){
                if(deliveryOfBall.wide_runs == 1 || deliveryOfBall.noball_runs ==1){
                    topEconomyBowlersRunsAndBalls[bowler] = [parseInt(runsInBall),0];
                }
                else{
                    topEconomyBowlersRunsAndBalls[bowler] = [parseInt(runsInBall),1];
                }
                
            }
            else{
                if(deliveryOfBall.wide_runs == 1 || deliveryOfBall.noball_runs ==1){
                    topEconomyBowlersRunsAndBalls[bowler] = [topEconomyBowlersRunsAndBalls[bowler][0]+parseInt(runsInBall),topEconomyBowlersRunsAndBalls[bowler][1]];
                }
                else{
                    topEconomyBowlersRunsAndBalls[bowler] = [topEconomyBowlersRunsAndBalls[bowler][0]+parseInt(runsInBall),topEconomyBowlersRunsAndBalls[bowler][1]+1];
                }
            }
        }
    }

    for(const bowler in topEconomyBowlersRunsAndBalls){
        if(!topEconomyBowlers[bowler]){
            topEconomyBowlers[bowler] = parseFloat((topEconomyBowlersRunsAndBalls[bowler][0]/(topEconomyBowlersRunsAndBalls[bowler][1]/6)).toFixed(2));
        }
    }


    const sortedBowlersEconomy = Object.entries(topEconomyBowlers).sort((key, value) => key[1] - value[1]);

    const top10Bowlers = Object.fromEntries(sortedBowlersEconomy.slice(0, 10));
    

    fs.writeFile('../public/output/4-top-10-economical-bowlers-in-2015.json', JSON.stringify(top10Bowlers,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

    console.log(top10Bowlers);

}