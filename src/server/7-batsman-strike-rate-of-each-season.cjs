const fs = require('fs');
const csv = require('csv-parser');

const iplDeliveriesData = [];
const matchIdAndSeasons = {};

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => {
    if(!matchIdAndSeasons[data.id]){
        matchIdAndSeasons[data.id] = data.season;
    }
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data',(data) => iplDeliveriesData.push(data))
    .on('end',() => {
        calculateBatsmanStrikeRateOfEachSeason(iplDeliveriesData,matchIdAndSeasons);
    })
  });

  function calculateBatsmanStrikeRateOfEachSeason(iplDeliveriesData,matchIdAndSeasons){

    const batsmanRunsAndBalls = {};
    const batsmanRunsAndBallsInSeason = {};
    const batsmanStrikeRateBySeason = {};

    for(const deliveryOfBall of iplDeliveriesData){
        const batsman = deliveryOfBall.batsman;
        const batsmanRuns = deliveryOfBall.batsman_runs;
        const matchId = deliveryOfBall.match_id;
        const matchYear = matchIdAndSeasons[matchId];
        if(!batsmanRunsAndBallsInSeason[batsman]){
            batsmanRunsAndBallsInSeason[batsman] = {};
        }
        if(!batsmanRunsAndBallsInSeason[batsman][matchYear]){
            batsmanRunsAndBallsInSeason[batsman][matchYear] = [parseInt(batsmanRuns),0];
        }
        else{
            if(deliveryOfBall.wide_runs == 0 && deliveryOfBall.noball_runs == 0){
                batsmanRunsAndBallsInSeason[batsman][matchYear] = [batsmanRunsAndBallsInSeason[batsman][matchYear][0]+parseInt(batsmanRuns), batsmanRunsAndBallsInSeason[batsman][matchYear][1]+1];
            }
            else{
                batsmanRunsAndBallsInSeason[batsman][matchYear] = [batsmanRunsAndBallsInSeason[batsman][matchYear][0]+parseInt(batsmanRuns), batsmanRunsAndBallsInSeason[batsman][matchYear][1]];
            }
            
        }
    }

    for(const batsman in batsmanRunsAndBallsInSeason){
        if(!batsmanStrikeRateBySeason[batsman]){ 
            batsmanStrikeRateBySeason[batsman] = {};
        }

        for(const year in batsmanRunsAndBallsInSeason[batsman]){
            if(!batsmanStrikeRateBySeason[batsman][year]){
                batsmanStrikeRateBySeason[batsman][year] = parseFloat(((batsmanRunsAndBallsInSeason[batsman][year][0]/batsmanRunsAndBallsInSeason[batsman][year][1])*100).toFixed(2));
            }
        }

    }


    fs.writeFile('../public/output/7-batsman-strike-rate-of-each-season.json', JSON.stringify(batsmanStrikeRateBySeason,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

    console.log(batsmanStrikeRateBySeason);

  }