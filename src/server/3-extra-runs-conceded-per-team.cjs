const fs = require('fs');
const csv = require('csv-parser');

const iplDeliveriesData = [];
const matchIdOfYear2016 = []

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => {
    if(data.season === '2016'){
        matchIdOfYear2016.push(data.id);
    }
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data',(data) => iplDeliveriesData.push(data))
    .on('end',() => {
        extraRunsConcededPerTeam(iplDeliveriesData,matchIdOfYear2016);
    })
  });


function extraRunsConcededPerTeam(iplDeliveriesData,matchIdOfYear2016){
    const extraRunsConceded = {};
    
    for(const deliveryOfBall of iplDeliveriesData){
        const matchId = deliveryOfBall.match_id;
        const bowlingTeam = deliveryOfBall.bowling_team;
        const extraRuns = deliveryOfBall.extra_runs;

        if(matchIdOfYear2016.includes(matchId)){
            if(!extraRunsConceded[bowlingTeam]){
                extraRunsConceded[bowlingTeam] = parseInt(extraRuns);
            }
            else{
                extraRunsConceded[bowlingTeam] = extraRunsConceded[bowlingTeam] + parseInt(extraRuns);
            }
        }
    }

    fs.writeFile('../public/output/3-extra-runs-conceded-per-team.json', JSON.stringify(extraRunsConceded,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });
    
    console.log(extraRunsConceded);
}