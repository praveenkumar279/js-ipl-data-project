const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    calculateMatchesWonPerTeam(iplMatchesData);
  });


  function calculateMatchesWonPerTeam(iplMatchesData){

    const matchesWonPerTeamPerYear = {}

    for (const match of iplMatchesData) {
        const matchYear = match.season;
        const matchWinnerTeam = match.winner;
        const matchResult = match.result;

        if(matchResult == 'normal'){
          if (!matchesWonPerTeamPerYear[matchWinnerTeam]) {
            matchesWonPerTeamPerYear[matchWinnerTeam] = {};
          }
          if (!matchesWonPerTeamPerYear[matchWinnerTeam][matchYear]) {
            matchesWonPerTeamPerYear[matchWinnerTeam][matchYear]=0;
          }
          matchesWonPerTeamPerYear[matchWinnerTeam][matchYear]++;
        }
      }

      
      fs.writeFile('../public/output/2-matches-won-per-team-per-year.json', JSON.stringify(matchesWonPerTeamPerYear,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

      console.log(matchesWonPerTeamPerYear);
  }