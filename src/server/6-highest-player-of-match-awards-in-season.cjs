const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    calculateHighestPLayerOfTheMatchAwards(iplMatchesData);
  });


function calculateHighestPLayerOfTheMatchAwards(iplMatchesData){
    const HighestPlayerOfMatchAwards = {};
    const playerOfMatchAwards = {};

    for(const match of iplMatchesData){
        const matchYear = match.season;
        const playerOfMatch = match.player_of_match;

        if(!playerOfMatchAwards[matchYear]){
            playerOfMatchAwards[matchYear] = {};
        }
        if(!playerOfMatchAwards[matchYear][playerOfMatch]){
            playerOfMatchAwards[matchYear][playerOfMatch] = 1;
        }
        else{
            playerOfMatchAwards[matchYear][playerOfMatch]++;
        }

    }

    for(const year in playerOfMatchAwards){
        let highestPlayerMatchAwards = null;
        let Awards = 0;

        for(const player in playerOfMatchAwards[year]){
            const awardsOfPlayer = playerOfMatchAwards[year][player];
    
            if (awardsOfPlayer > Awards) {
                highestPlayerMatchAwards = player;
                Awards = awardsOfPlayer;
            }
        }
        HighestPlayerOfMatchAwards[year] = {highestPlayerMatchAwards,Awards};
    }
    

    fs.writeFile('../public/output/6-highest-player-of-match-awards-in-season.json', JSON.stringify(HighestPlayerOfMatchAwards,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

    console.log(HighestPlayerOfMatchAwards);
}