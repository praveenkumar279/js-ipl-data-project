const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/deliveries.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    bestEconomyBowlerInSuperOvers(iplMatchesData);
  });

function bestEconomyBowlerInSuperOvers(iplMatchesData){
    const bowlersBallsAndRuns = {};
    const bowlersEconomy = {};

    for(const deliveryOfBall of iplMatchesData){
        const matchInnings = deliveryOfBall.inning;
        const bowlerInSuperOver = deliveryOfBall.bowler;
        const ballInSuperOver = deliveryOfBall.ball;
        const runsInSuperOver = deliveryOfBall.total_runs - deliveryOfBall.bye_runs - deliveryOfBall.legbye_runs ;

        if(deliveryOfBall.is_super_over == 1){
            if(!bowlersBallsAndRuns[bowlerInSuperOver]){
                if(deliveryOfBall.wide_runs == 1 || deliveryOfBall.noball_runs ==1){
                    bowlersBallsAndRuns[bowlerInSuperOver] = [parseInt(runsInSuperOver),0];
                }
                else{
                    bowlersBallsAndRuns[bowlerInSuperOver] = [parseInt(runsInSuperOver),1];
                }   
            }
            else{
                if(deliveryOfBall.wide_runs == 1 || deliveryOfBall.noball_runs ==1)
                {
                    bowlersBallsAndRuns[bowlerInSuperOver] = [bowlersBallsAndRuns[bowlerInSuperOver][0]+parseInt(runsInSuperOver),bowlersBallsAndRuns[bowlerInSuperOver][1]];
                }
                else{
                    bowlersBallsAndRuns[bowlerInSuperOver] = [bowlersBallsAndRuns[bowlerInSuperOver][0]+parseInt(runsInSuperOver),bowlersBallsAndRuns[bowlerInSuperOver][1]+1];
                }
            }
        }

    }

    for(const bowler in bowlersBallsAndRuns){
        if(!bowlersEconomy[bowler]){
            bowlersEconomy[bowler] = parseFloat((bowlersBallsAndRuns[bowler][0]/(bowlersBallsAndRuns[bowler][1]/6)).toFixed(2));
        }
    }


    const sortedBestEconomy = Object.entries(bowlersEconomy).sort((key, value) => key[1] - value[1]);
    const bestBowler = Object.fromEntries(sortedBestEconomy.slice(0, 5));



    fs.writeFile('../public/output/9-bowler-with-best-economy-in-super-overs.json', JSON.stringify(bestBowler,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

    
    console.log(bestBowler);
}