const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/deliveries.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    calculateNumberOfTimesPlayerDismissedByAnotherPlayer(iplMatchesData);
  });


function calculateNumberOfTimesPlayerDismissedByAnotherPlayer(iplMatchesData){
    const playerDismissals = {}

    for(const delivery of iplMatchesData){
        const bowler = delivery.bowler;
        const playerDismissed = delivery.player_dismissed;
        const dismissedType = delivery.dismissal_kind;
        const runOutFielder = delivery.fielder;
        if(!playerDismissals[playerDismissed]){
            playerDismissals[playerDismissed] = {};
        }
        if(!playerDismissals[playerDismissed][bowler] && dismissedType !== "run out"){
            playerDismissals[playerDismissed][bowler] = 1;
        }
        else if(dismissedType !== "run out"){
            playerDismissals[playerDismissed][bowler]++;
        }
        if(!playerDismissals[playerDismissed][bowler] && dismissedType === "run out"){
            playerDismissals[playerDismissed][runOutFielder] = 1;
        }
        else if(dismissedType === "run out"){
            playerDismissals[playerDismissed][runOutFielder]++;
        }
    }


    const highestTimesPlayerDismissed = {};

    for(const batsmen in playerDismissals){
        let highestDismissedBowler = null;
        let highestDismissals = 0;
        if(batsmen !== ''){
            for(const bowler in playerDismissals[batsmen]){
                let bowlerDismissals = playerDismissals[batsmen][bowler];
                    if(bowlerDismissals > highestDismissals){
                        highestDismissals = bowlerDismissals;
                        highestDismissedBowler = bowler;
                    }
            }
            highestTimesPlayerDismissed[batsmen] = {'dismissed_by' : highestDismissedBowler, 'NoOfTimesDismissed' : highestDismissals};
        }
        
    }

    const highestDismissedPlayer = Object.fromEntries(Object.entries(highestTimesPlayerDismissed).sort((key, value) => value[1]['NoOfTimesDismissed'] - key[1]['NoOfTimesDismissed']).slice(0,5));


    fs.writeFile('../public/output/8-highest-number-times-one-player-dismissed-by-another-player.json', JSON.stringify(highestDismissedPlayer,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });
    
    
    console.log(highestDismissedPlayer);
}
