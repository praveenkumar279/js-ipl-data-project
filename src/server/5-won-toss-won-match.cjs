const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    calculateWonTossAndWonMatch(iplMatchesData);
  });


function calculateWonTossAndWonMatch(iplMatchesData){
    const NumberOfTimesWonTossAndWonMatch = {};

    for(const match of iplMatchesData){
        const tossWinner = match.toss_winner;
        const matchWinner = match.winner;

        if(tossWinner === matchWinner){
            if(!NumberOfTimesWonTossAndWonMatch[tossWinner]){
                NumberOfTimesWonTossAndWonMatch[tossWinner] = 1;
            }
            else{
                NumberOfTimesWonTossAndWonMatch[tossWinner]++;
            }
        }
        
    }


    fs.writeFile('../public/output/5-won-won-match-won-team.json', JSON.stringify(NumberOfTimesWonTossAndWonMatch,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });

    console.log(NumberOfTimesWonTossAndWonMatch);
}

module.exports = calculateWonTossAndWonMatch;