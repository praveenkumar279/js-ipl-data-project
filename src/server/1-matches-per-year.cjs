const fs = require('fs');
const csv = require('csv-parser');

const iplMatchesData = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => iplMatchesData.push(data))
  .on('end', () => {
    calculateMatchesPerYear(iplMatchesData);
  });



function calculateMatchesPerYear(iplMatchesData){
    let matchesPerYear = {}
    for(const match of iplMatchesData){
        const matchYear = match.season;
        if(!matchesPerYear[matchYear]){
            matchesPerYear[matchYear] = 0;
        }
        matchesPerYear[matchYear]++;
    }

    fs.writeFile('../public/output/1-matches-per-year.json', JSON.stringify(matchesPerYear,null,2),'utf-8',function(err){
        if(err){
            console.log("unable to write into JSON file");
            return err;
        }
        console.log("Successfully written onto JSON file");
    });
    
    console.log(matchesPerYear);

}

module.exports = calculateMatchesPerYear;



  

  
